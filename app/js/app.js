$('.header-hamburger').click(function(){
    $('.main-menu').addClass('active');
    $('.main-menu-overlay').addClass('active');
})

$('.main-menu-overlay').click(function(){
    $('.main-menu').removeClass('active');
    $('.main-menu-overlay').removeClass('active');
})

$('.cate-filter__item').click(function() {
    if ($(this).hasClass('cate-filter__item--active')) {
        $(this).find('.cate-filter__sub-items').slideUp();
        $(this).removeClass('cate-filter__item--active');
    } else {
        $('.cate-filter__item').removeClass('cate-filter__item--active');
        $('.cate-filter__sub-items').slideUp();
        $(this).find('.cate-filter__sub-items').slideDown();
        $(this).addClass('cate-filter__item--active');
    }
})

if (window.innerWidth < 768) {
    $('.main-menu__item').not('.main-menu__item--no-child').click(function() {
        if ($(this).hasClass('main-menu__item--active')) {
            $(this).find('.main-menu__inner').slideUp();
            $(this).removeClass('main-menu__item--active');
        } else {
            $('.main-menu__item').removeClass('main-menu__item--active');
            $('.main-menu__inner').slideUp();
            $(this).find('.main-menu__inner').slideDown();
            $(this).addClass('main-menu__item--active');
        }
    })
}


$('.banner__items').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: true,
    dots: true,
    autoplay: true,
    autoplaySpeed: 3000,
  });

$('.main-slider-main').slick({
  slidesToShow: 1,
  slidesToScroll: 1,
  arrows: false,
  dots: false,
  infinite: false,
  fade: true,
  asNavFor: '.main-slider-sub'
});

$('.main-slider-sub').slick({
    slidesToShow: 6,
    slidesToScroll: 1,
    asNavFor: '.main-slider-main',
    dots: false,
    arrows: false,
    infinite: false,
    focusOnSelect: true
});

$('.main-slider__number').on('click', function(){
    $('.main-slider-overlay').show();

    setTimeout(function(){
        $('.main-slider-main-overlay').slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: true,
            dots: false,
            infinite: true,
            fade: true,
            asNavFor: '.main-slider-sub-overlay'
        });
        
        $('.main-slider-sub-overlay').slick({
            slidesToShow: 8,
            slidesToScroll: 1,
            asNavFor: '.main-slider-main-overlay',
            dots: false,
            arrows: true,
            infinite: true,
            focusOnSelect: true
        });
    }, 200);
});


$('.main-slider__view-photo').on('click', function(){
    $('.main-slider-overlay').show();

    setTimeout(function(){
        $('.main-slider-main-overlay').slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: true,
            dots: false,
            infinite: true,
            fade: true,
            asNavFor: '.main-slider-sub-overlay'
        });
        
        $('.main-slider-sub-overlay').slick({
            slidesToShow: 8,
            slidesToScroll: 1,
            asNavFor: '.main-slider-main-overlay',
            dots: false,
            arrows: true,
            infinite: true,
            focusOnSelect: true
        });
    }, 200);
});

$('.main-slider-overlay__close').on('click', function(){
    $('.main-slider-overlay').hide();
    $('.main-slider-main-overlay').slick('unslick');
    $('.main-slider-sub-overlay').slick('unslick');
});

$('.product-owners__items').slick({
    slidesToShow: 3,
    slidesToScroll: 3,
    arrows: true,
    responsive: [
        {
          breakpoint: 768,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
          }
        }
      ]
});

$('.cate-product-actions__switch-checkbox').on('change', function() {
    if ($(this).prop('checked')) {
        $('.cate-product__items').addClass('cate-product__items--room-view');
    } else {
        $('.cate-product__items').removeClass('cate-product__items--room-view');
    }
})