var gulp = require('gulp');
var sass = require('gulp-sass');
var path = require('path');
var autoprefixer = require('gulp-autoprefixer');
var sourcemaps = require('gulp-sourcemaps');
var watch = require('gulp-watch');
var uglify = require('gulp-uglify');
var rename = require('gulp-rename');
var pump = require('pump');
var concat = require('gulp-concat');
var browserSync = require('browser-sync').create();
var cleanCSS = require('gulp-clean-css');


gulp.task('sass', function () {
    return gulp.src('./app/sass/**/*.scss')
     .pipe(sourcemaps.init())
     .pipe(sass().on('error', sass.logError))
     .pipe(autoprefixer({
        browsers: ['last 2 versions', 'ie >= 9']
    }))
     .pipe(sourcemaps.write())
     .pipe(gulp.dest('public/css'));
});

// gulp.task('scss-deploy', ['sass'], function () {
//     return gulp.src(['public/css/*.css'])
//         .pipe(cleanCSS())
//         .pipe(rename('main.css'))
//         .pipe(gulp.dest('./public/css'))
// });

gulp.task('minify-css', () => {
    return gulp.src('public/css/*.css')
      .pipe(cleanCSS({compatibility: 'ie8'}))
      .pipe(rename('main.min.css'))
      .pipe(gulp.dest('./public/css'));
});

gulp.task('js', function(){
    return gulp.src([
        './app/js/jQuery.js',
        './app/js/plugins/*js',
        './app/js/*.js'
    ])
        .pipe(sourcemaps.init())
        .pipe(concat('main.js'))
        .pipe(gulp.dest('public/js'))
});

gulp.task('minify-js', function(){
    return gulp.src('./public/js/*.js')
        .pipe(rename('main.min.js'))
        .pipe(uglify())
        .pipe(gulp.dest('public/js'));
});


gulp.task('browser-sync', ['sass', 'js'], function() {

    browserSync.init({
        server: "./"
    });

    gulp.watch("./app/sass/*.scss", ['sass']);
    gulp.watch("./app/js/*.js", ['js']);
    
    gulp.watch("*.html").on('change', browserSync.reload);
    gulp.watch("./public/css/*.css").on('change', browserSync.reload);
    gulp.watch("./public/js/*.js").on('change', browserSync.reload);
});

// gulp.task('dev', function () {
//     gulp.watch('app/less/**/*.less', ['less']);
//     gulp.watch('app/js/*.js', ['js']);
//     // Other watchers
// })

gulp.task('build', ['minify-css', 'minify-js']);

gulp.task('dev', ['browser-sync']);